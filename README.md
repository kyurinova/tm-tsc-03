# TASK MANAGER

## DEVELOPER INFO

name: Kseniia Yurinova

e-mail: ks093050@mail.ru

## TASK MANAGER

SCREENSHOTS

https://yadi.sk/d/CkJLmzAuDAiRDQ?w=1

## HARDWARE

CPU: i5

RAM: 16G

SSD: 512GB

## SOFTWARE

System: Windows 10

Version JDK: 1.8.0_281

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
